package main

import "time"
import "fmt"
import "math/rand"
const ARRAY_SIZE = 10

func randomInit(array *[ARRAY_SIZE]int) {
  const MAX_RANDOM_NUMBER = 1000
  randomSource := rand.NewSource(time.Now().UnixNano())
  random := rand.New(randomSource)
	var i int

	for i = 0; i < ARRAY_SIZE; i++ {
		array[i] = random.Intn(MAX_RANDOM_NUMBER)
	}
}

func insertionSort(array *[ARRAY_SIZE]int) {
  var i, j int

  // Starts from the second position, because we assume
  // the first element is in the ordered list already
  for i = 1; i < ARRAY_SIZE; i++ {
    // Uses the tempIndex to follow the item while swaping,
    // and not getting lost the i position 
    var tempIndex = i
    
    // While the number checked with the tempIndex is
    // smaller than the one on the left, we keep swaping
    for j = i - 1; j >= 0 && array[tempIndex] < array[j]; j-- {
      var temp = array[tempIndex]

      array[tempIndex] = array[j]
      array[j] = temp
      tempIndex --
    }
  }
}

func main() {
  var array [ARRAY_SIZE] int
  
  randomInit(&array)

	fmt.Println(array)
  
  insertionSort(&array)

	fmt.Println(array)
}

